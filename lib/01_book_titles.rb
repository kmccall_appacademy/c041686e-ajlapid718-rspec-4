class Book
  attr_accessor :title

  def title
    titleize(@title)
  end

private

  def titleize(title)
    title_words = title.capitalize.split(" ")
    title_words.map { |word| my_capitalize(word) }.join(" ")
  end

  def my_capitalize(word)
    words_to_skip = ["and", "in", "the", "of", "a", "an"]
    words_to_skip.include?(word) ? word : word.capitalize
  end

end
