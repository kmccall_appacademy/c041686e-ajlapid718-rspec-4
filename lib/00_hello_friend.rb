class Friend
  def greeting(who=nil)
    if who
      "Hello, #{who}!"
    else
        "Hello!"
    end
  end
end

# class Friend
#   def greeting(who=nil)
#     who ? "Hello, #{who}!" : "Hello!"
#   end
# end
